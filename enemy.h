#ifndef ENEMY_H
#define ENEMY_H

#include <stdlib.h>
#include "./sprites.h"

struct enemyPos {
   int posY;
   int posX;
};

struct enemyPos enemy1;
struct enemyPos enemy2;
struct enemy_killed killed;

bool is_enemy_alive(struct enemy_killed killed)
{
   if (killed.enemy1 == false && killed.enemy2 == false)
   {
      return true;
   }
}

void *move_enemy()
{
   killed.enemy1 = false;
   killed.enemy2 = false;

   long frames = 0; /* This behaves kinda like a "tick". Each one takes 0.19s */
   long currentFrames1 = 0;
   long currentFrames2 = 0;
   bool setkillattribs1 = false;
   bool setkillattribs2 = false;
   /* I don't know why "||" works better than "&&", but it does :) */
   while ((playerPosY != enemy1.posY || playerPosX != enemy1.posX) || (playerPosY != enemy2.posY || playerPosX != enemy2.posX))
   {
      frames += 1;
      if (player_move == false) {
         erase_enemy(enemy1.posY, enemy1.posX);
         erase_enemy(enemy2.posY, enemy2.posX);

         if (killed.enemy1 == false) {
            if (enemy1.posY < LINES && enemy1.posY >= 0) {
               if (playerPosY > enemy1.posY) {
                  enemy1.posY += 1;
               }

               else if (playerPosY < enemy1.posY) {
                  enemy1.posY -= 1;
               }
            }

            if (enemy1.posX < COLS && enemy1.posX >= 0) {
               if (playerPosX > enemy1.posX) {
                  enemy1.posX += 2;
               }

               else if (playerPosX < enemy1.posX) {
                  enemy1.posX -= 2;
               }
            }
         }

         if (killed.enemy2 == false) {
            if (enemy2.posY < LINES && enemy2.posY >= 0) {
               if (playerPosY > enemy2.posY) {
                  enemy2.posY += 2;
               }

               else if (playerPosY < enemy2.posY) {
                  enemy2.posY -= 2;
               }
            }
         }

         if (killed.enemy1 == false) draw_enemy(enemy1.posY, enemy1.posX);
         if (killed.enemy2 == false) draw_enemy(enemy2.posY, enemy2.posX);
         refresh();

         if (killed.enemy1 == false) {
            if ((playerPosY == enemy1.posY && playerPosX == enemy1.posX)
            || (playerPosY == enemy1.posY && playerPosX +1 == enemy1.posX)
            || (playerPosY == enemy1.posY && playerPosX -1 == enemy1.posX)
            || (playerPosY == enemy1.posY && playerPosX +2 == enemy1.posX)
            || (playerPosY == enemy1.posY && playerPosX -2 == enemy1.posX))
            {
               gameover_scr();
               break;
            }
         }

         if (killed.enemy2 == false) {
            if ((playerPosY == enemy2.posY && playerPosX == enemy2.posX -1)
            || (playerPosY == enemy2.posY && playerPosX == enemy2.posX)
            || (playerPosY == enemy2.posY +1 && playerPosX == enemy2.posX -1)
            || (playerPosY == enemy2.posY && playerPosX == enemy2.posX +1)
            || (playerPosY == enemy2.posY -1 && playerPosX == enemy2.posX +1)
            || (playerPosY == enemy2.posY -1 && playerPosX == enemy2.posX -1)
            || (playerPosY == enemy2.posY -1 && playerPosX == enemy2.posX)
            || (playerPosY == enemy2.posY +1 && playerPosX == enemy2.posX)
            || (playerPosY == enemy2.posY +1 && playerPosX == enemy2.posX +1))
            {
               gameover_scr();
               break;
            }
         }
      }

      player_move = true;
      system("sleep 0.19s");
      player_move = false;

      /* respawns enemy */
      if (!is_enemy_alive(killed)) {
         player_move = true;

         if (killed.enemy1 == true) {
            player_move = false;

            if (setkillattribs1 == false) {
               currentFrames1 = frames;
               setkillattribs1 = true;
            }

            if (frames == currentFrames1 + 20) {
               erase_enemy(enemy1.posY, enemy1.posX);
               enemy1.posY = 4;
               enemy1.posX = 10;

               currentFrames1 = 0;
               setkillattribs1 = false;
               killed.enemy1 = false;
            }
         }

         if (killed.enemy2 == true) {
            player_move = false;

            if (setkillattribs2 == false) {
               currentFrames2 = frames; 
               setkillattribs2 = true; // With this, "currentFrames" won't reset its value
            }

            if (frames == currentFrames2 + 15) {
               erase_enemy(enemy2.posY, enemy2.posX);
               enemy2.posY = 7;
               enemy2.posX = 10;

               currentFrames2 = 0;
               setkillattribs2 = false;
               killed.enemy2 = false;
            }
         }
      }

      update_scr(enemy1.posY, enemy1.posX, enemy2.posY, enemy2.posX);
   }

   return (void *) 0;
}

#endif
